class User
  class Device
    class Activator
      include ActiveModel::Validations

      attr_reader :user
      attr_reader :activation_code
      attr_reader :device

      validates :activation_code, presence: true
      validate :device_existance, if: 'activation_code.present?'

      def initialize(user, activation_code)
        @user = user
        @activation_code = activation_code
      end

      def activate!
        return false if invalid?

        device = Device.find_by! activation_code: activation_code

        Device.transaction do
          user.devices << device
          device.clear_activation_code!
          device.ensure_authentication_token!
        end

        device
      end

      private

      def device_existance
        return if Device.exists? activation_code: activation_code
        errors.add(:activation_code, 'does not match with any device')
      end
    end
  end
end
