class User
  class Device < ActiveRecord::Base
    belongs_to :user

    validates :model, :uid, presence: true
    validates :uid, uniqueness: true

    # Device activated if user would enter activation code and we, after
    # that, attach device to user account.
    def activated?
      user.present?
    end

    def ensure_activation_code!
      ensure_unique_field! :activation_code do
        SecureRandom.hex(2).upcase
      end
    end

    def clear_activation_code!
      self.activation_code = nil
      save validate: false
    end

    def ensure_authentication_token!
      ensure_unique_field! :authentication_token do
        SecureRandom.hex
      end
    end

    private

    def ensure_unique_field!(field, &block)
      attempts = 100

      unique_value = loop do
        attempts -= 1
        fail "Cannot generate #{field}" if attempts == 0

        random_value = block.call
        break random_value unless self.class.exists?(field => random_value)
      end

      send "#{field}=", unique_value
      save validate: false
    end
  end
end
