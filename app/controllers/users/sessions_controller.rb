module Users
  class SessionsController < ApplicationController
    def new
      logout if logged_in?
    end

    def create
      @user = login(params[:email], params[:password])
      if @user.present?
        redirect_back_or_to(root_path)
      else
        flash[:error] = 'Invalid name or password.'
        render action: 'new'
      end
    end

    def destroy
      logout
      redirect_to root_path
    end
  end
end
