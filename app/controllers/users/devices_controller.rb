module Users
  class DevicesController < ApplicationController
    before_action :authorize_user

    def activate
      activator = User::Device::Activator.new(current_user, params[:activation_code])

      if activator.valid?
        activator.activate!
        flash[:notice] = 'Device attached to your account'
      else
        flash[:error] = activator.errors.full_messages.join(', ')
      end

      redirect_to action: :index
    end

    private

    def authorize_user
      redirect_to user_login_path unless logged_in?
    end
  end
end
