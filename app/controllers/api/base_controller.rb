module API
  class BaseController < ActionController::Base
    protect_from_forgery with: :null_session

    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

    before_action :check_for_json_request

    private

    def render_not_found
      render nothing: true, status: 404
    end

    def check_for_json_request
      return if request.format.json?
      render nothing: true, status: :not_acceptable
    end
  end
end
