module API
  class DevicesController < BaseController
    def create
      device = User::Device.create device_params

      if device.persisted?
        device.ensure_activation_code!

        render json: { activation_code: device.activation_code }, status: 201
      else
        render json: device.errors, status: 422
      end
    end

    def authenticate
      device = User::Device.find_by! uid: device_params[:uid]

      if device.activated?
        render json: { authentication_token: device.authentication_token }, status: 200
      else
        render nothing: true, status: 401
      end
    end

    private

    def device_params
      params.require(:device).permit(:model, :uid)
    end
  end
end
