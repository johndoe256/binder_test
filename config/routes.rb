Rails.application.routes.draw do
  namespace :api do
    resources :devices, only: :create do
      post :authenticate, on: :collection
    end
  end

  namespace :users, as: :user, path: '/' do
    get '/login' => 'sessions#new'
    post '/login' => 'sessions#create'
    delete '/logout' => 'sessions#destroy'

    resources :devices, only: :index do
      post :activate, on: :collection
    end
  end

  root 'users/devices#index'
end
