class CreateUserDevices < ActiveRecord::Migration
  def change
    create_table :user_devices do |t|
      t.references :user, index: true

      t.string :model, null: false
      t.string :uid, null: false

      t.string :activation_code
      t.string :authentication_token

      t.timestamps null: false
    end

    add_foreign_key :user_devices, :users
    add_index :user_devices, :uid, unique: true
    add_index :user_devices, :activation_code
  end
end
