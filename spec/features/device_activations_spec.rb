require 'rails_helper'

feature 'DeviceActivations', type: :feature do
  before do
    user = User.create! email: 'testmail@gmail.com', password: '111111'
    visit '/login'
    fill_in 'email', with: user.email
    fill_in 'password', with: '111111'
    click_button 'Login'
  end

  scenario 'empty activation code' do
    click_button 'activate'

    expect(page).to have_text 'Activation code can\'t be blank'
  end

  scenario 'Invalid activation code' do
    fill_in 'activation_code', with: 'ABCD'
    click_button 'activate'

    expect(page).to have_text 'Activation code does not match with any device'
  end

  scenario 'Proper activation code' do
    device = User::Device.create! model: 'Foo', uid: '00:0a:95:9d:68:16'
    device.ensure_activation_code!

    fill_in 'activation_code', with: device.activation_code
    click_button 'activate'

    expect(page).to have_text 'Device attached to your account'
  end
end
