require 'rails_helper'

feature 'UserSessions', type: :feature do
  scenario 'Invalud name or password' do
    visit '/login'
    fill_in 'email', with: 'foo@bar.com'
    fill_in 'password', with: '111111'
    click_button 'Login'

    expect(page).to have_text 'Invalid name or password'
  end

  scenario 'Logout' do
    user = User.create! email: 'foo@bar.com', password: '111111'
    visit '/login'
    fill_in 'email', with: user.email
    fill_in 'password', with: '111111'
    click_button 'Login'
    click_link 'Logout'

    expect(page).to_not have_text 'Hello, foo@bar.com!'
  end
end
