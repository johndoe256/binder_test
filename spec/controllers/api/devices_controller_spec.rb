require 'rails_helper'

describe API::DevicesController, type: :controller do
  context 'POST create' do
    it 'returns proper status and activation code' do
      post :create, device: { model: 'Unknown tablet', uid: '00:0a:95:9d:68:16' }, format: :json

      device = User::Device.first
      data = JSON.parse(response.body)

      expect(response.status).to eq 201
      expect(User::Device.count).to eq 1
      expect(device.activation_code).to_not be_nil
      expect(data['activation_code']).to eq device.activation_code
    end

    it 'returns 422 and errors description if device data is invalid' do
      post :create, device: { model: 'Foo tablet' }, format: :json

      data = JSON.parse(response.body)

      expect(response.status).to eq 422
      expect(User::Device.count).to eq 0
      expect(data).to eq('uid' => ['can\'t be blank'])
    end

    it 'returns 422 and errors description if device already exists' do
      post :create, device: { model: 'Unknown tablet', uid: '00:0a:95:9d:68:16' }, format: :json
      post :create, device: { model: 'Unknown tablet', uid: '00:0a:95:9d:68:16' }, format: :json

      data = JSON.parse(response.body)

      expect(response.status).to eq 422
      expect(User::Device.count).to eq 1
      expect(data).to eq('uid' => ['has already been taken'])
    end
  end

  context 'POST authenticate' do
    context 'device is not registered' do
      it 'returns 404' do
        post :authenticate, device: { model: 'Foo', uid: '00:0a:95:9d:68:16' }, format: :json

        expect(response.status).to eq 404
      end
    end

    context 'device is not activated' do
      it 'returns 401' do
        device = User::Device.create! model: 'Foo', uid: '00:0a:95:9d:68:16'
        device.ensure_activation_code!
        post :authenticate, device: { model: device.model, uid: device.uid }, format: :json

        expect(response.status).to eq 401
      end
    end

    context 'device activated' do
      it 'returns 200 and authentication_token' do
        user = User.create! email: 'test111@mail.com', password: '111111'
        device = User::Device.create! model: 'Foo', uid: '00:0a:95:9d:68:16'
        device.ensure_activation_code!

        activator = User::Device::Activator.new(user, device.activation_code)
        activator.activate!

        post :authenticate, device: { model: device.model, uid: device.uid }, format: :json

        data = JSON.parse(response.body)
        device.reload

        expect(response.status).to eq 200
        expect(data['authentication_token']).to eq device.authentication_token
      end
    end
  end
end
