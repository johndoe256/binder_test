require 'rails_helper'

describe API::BaseController do
  controller(API::BaseController) do
    def index
      render json: { message: 'some content' }
    end
  end

  describe 'handling request formats' do
    it 'returns 200 when JSON' do
      get :index, format: :json
      expect(response.status).to eq 200
    end

    it 'returns 406 for other' do
      get :index, format: :html
      expect(response.status).to eq 406
    end
  end
end
